// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore";
import { getDocs,addDoc, collection } from "firebase/firestore"; 
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBtCvuJwa5XSL5aVTrJT-xrSrE9DBmPNEA",
  authDomain: "besties-21b76.firebaseapp.com",
  projectId: "besties-21b76",
  storageBucket: "besties-21b76.appspot.com",
  messagingSenderId: "518786678592",
  appId: "1:518786678592:web:8e870c8bf5531efc3b84a5",
  measurementId: "G-YYWSBJVKDD"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const db = getFirestore(app);

export const createMessage = async (message) => {
    const docRef = await addDoc(collection(db, "messages"), {
        text: message.text,
        createdAt: message.createdAt,
        user: message.user
    });

    return docRef;
};

export const getAllMessages = async () => {
    const querySnapshot = await getDocs(collection(db, "messages"));

    return querySnapshot;
};


import { createApp } from 'vue'
import './index.css'
import App from './App.vue'
import VueSimpleAlert from "vue3-simple-alert";

const app = createApp(App);

app.use(VueSimpleAlert);
app.mount('#app')
